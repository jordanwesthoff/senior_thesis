senior_thesis
=============

Repository for all non-proprietary code for my senior thesis at the Rochester Institute of Technology, 2014.
Jordan Westhoff, 2014

-----------------------
### stack_kickstart.cfg
  -> This is the developing kickstart for all CentOS 6.5 nodes in the stack.
