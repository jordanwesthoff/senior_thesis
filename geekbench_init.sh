#!/bin/bash

# Install the dependencies for GB
sudo apt-get install libc6:i386 libstdc++6:i386


cd /home/jordan/Desktop
wget http://logcat.student.rit.edu/Public%20Share/Geekbench-3.2.0-Linux.tar.gz
tar -xzvf Geekbench-3.2.0-Linux.tar.gz
rm Geekbench-3.2.0-Linux.tar.gz

sleep 3
cd dist
cd Geekbench-3.2.0-Linux
./geekbench -r jaw4290@rit.edu jxrva-3bau5-n6nt5-pi36h-nbfc6-kkt2x-eh6qc-24seg-2ccps
./geekbench