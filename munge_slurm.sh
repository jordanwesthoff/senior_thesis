#!/bin/bash

USER=whoami
FULLPATH=/home/$USER/Desktop/munge_install


sudo mkdir $FULLPATH
cd $FULLPATH

# Fetch the MUNGE repo from my personal server
wget http://logcat.student.rit.edu/Public%20Share/munge-0.5.11.tar.bz2

# Install dependencies
sudo yum -y install openmpi-devel pam-devel hwloc-devel rrdtool-devel ncurses-devel munge-devel
sudo yum -y install bzip2-devel
sudo yum -y install openssl-devel

# Built the binary RPM
rpmbuild -tb --clean munge-0.5.11.tar.bz2

# Install the binary RPM
rpm -ivh munge-0.5.11-1.i386.rpm munge-devel-0.5.11-1.i386.rpm munge-libs-0.5.11-1.i386.rpm

# Configure MUNGE and make and install.
./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var && make && sudo make install